import React from 'react';
import {Badge, Button} from 'reactstrap';
import {connect} from "react-redux";

class Counter extends React.Component {
    render() {
        return (
            <div>
                <Button color="primary" outline>
                    Actions <Badge color="secondary">{this.props.counter}</Badge>
                </Button>
            </div>
        );
    }
}

const mapStateToProps = ({counter}) => ({
    counter
});

export default connect(mapStateToProps)(Counter);
