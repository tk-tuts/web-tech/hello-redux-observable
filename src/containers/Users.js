import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardBody, CardImg, CardTitle, Input, InputGroup, InputGroupAddon} from 'reactstrap';
import {connect} from "react-redux";
import {fetchUser} from "../actions";

class Users extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username: ''
        };

        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleKeyPress(e) {
        if (e.key === 'Enter') {
            this.props.fetchUser(e.target.value);
        }
    }

    handleChange(e) {
        this.setState({username: e.target.value});
    }

    render() {
        return (
            <div>
                <h1>Fetch Users</h1>
                <InputGroup>
                    <InputGroupAddon addonType="prepend">@</InputGroupAddon>
                    <Input placeholder="username"
                           onKeyPress={this.handleKeyPress}
                           value={this.state.username}
                           onChange={this.handleChange}/>
                </InputGroup>
                {this.props.users.user &&
                <Card className="profileCard">
                    <CardImg top width="100%" src={this.props.users.user.imageUrl} alt="Card image cap" />
                    <CardBody>
                        <CardTitle>{this.props.users.user.name}</CardTitle>
                    </CardBody>
                </Card>
                }
            </div>
        );
    }
}

Users.propTypes = {
    fetchUser: PropTypes.func.isRequired
};

const mapStateToProps = ({users}) => ({
    users
});

const mapDispatchToProps = {
    fetchUser
};

export default connect(mapStateToProps, mapDispatchToProps)(Users);
