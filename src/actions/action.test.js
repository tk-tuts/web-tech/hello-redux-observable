import {addTodo} from "../actions";
import {ADD_TODO} from "../constants/action-types";

describe('actions', () => {
    it('dispatches ADD_TODO event', () => {
        expect(addTodo('todo 1'))
            .toEqual({
                type: ADD_TODO,
                text: 'todo 1'
            });
    });
});
