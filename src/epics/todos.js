import {delay, mapTo} from 'rxjs/operators';
import {ADD_TODO, INCREMENT_COUNTER} from '../constants/action-types';
import {ofType} from "redux-observable";

export const addTodoEpic = action$ => action$.pipe(
    ofType(ADD_TODO),
    delay(1000),
    mapTo({type: INCREMENT_COUNTER})
);
