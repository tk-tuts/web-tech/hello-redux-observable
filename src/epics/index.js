import {combineEpics} from "redux-observable";
import {addTodoEpic} from "./todos";
import {fetchUserEpic} from "./users";

export default combineEpics(
    addTodoEpic,
    fetchUserEpic
);
