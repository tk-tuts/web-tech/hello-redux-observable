import {FETCH_USER} from "../constants/action-types";
import {map, switchMap} from "rxjs/operators";
import {ofType} from "redux-observable";
import {fetchUserFulfiled} from "../actions";

export const fetchUserEpic = (action$, state$, {getJSON}) => action$.pipe(
    ofType(FETCH_USER),
    switchMap(action =>
        getJSON(`https://api.github.com/users/${action.username}`).pipe(
            map(res => fetchUserFulfiled(res))
        )
    )
);
