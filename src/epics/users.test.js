import {TestScheduler} from "rxjs/testing";
import {fetchUserEpic} from "./users";
import {expect} from "chai";

const testScheduler = new TestScheduler((actual, expected) => {
    expect(actual).deep.equal(expected);
});

describe('users epic', () => {
    it('FETCH_USER --> FETCH_USER_FULFILLED', () => {
        testScheduler.run(({hot, cold, expectObservable}) => {
            const action$ = hot('-a', {
                a: {type: 'FETCH_USER', username: 'test'}
            });

            const state$ = null;
            const dependencies = {
                getJSON: url => cold('--a', {
                    a: {url}
                })
            };

            const output$ = fetchUserEpic(action$, state$, dependencies);

            expectObservable(output$).toBe('---a', {
                a: {
                    type: 'FETCH_USER_FULFILLED',
                    payload: {
                        url: 'https://api.github.com/users/test'
                    }
                }
            })
        });
    });
});
