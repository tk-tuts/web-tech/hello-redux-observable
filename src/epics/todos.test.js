import {ADD_TODO, INCREMENT_COUNTER} from "../constants/action-types";
import {TestScheduler} from "rxjs/testing";
import {expect} from "chai";
import {addTodoEpic} from "./todos";

const testScheduler = new TestScheduler((actual, expected) => {
    // expect(actual).toBe(expected);
    expect(actual).deep.equal(expected);
});

describe('todos epic', () => {
    it('ADD_TODO --> INCREMENT_COUNTER', () => {
        testScheduler.run(helpers => {
           const {hot, cold, expectObservable} = helpers;

           const action$ = hot('-a', {
               a: {type: ADD_TODO, text: 'test'}
           });

           const output$ = addTodoEpic(action$);
           expectObservable(output$).toBe('1000ms -a', {
               a: {
                   type: INCREMENT_COUNTER
               }
           })
        });
    });
});
