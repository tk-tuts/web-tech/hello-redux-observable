import React from 'react';
import Footer from './Footer';
import AddTodo from '../containers/AddTodo';
import VisibleTodoList from '../containers/VisibleTodoList';
import Counter from "../containers/Counter";
import Users from "../containers/Users";

const App = () => (
    <div>
        <h1>TODO</h1>
        <AddTodo />
        <VisibleTodoList />
        <Footer />
        <Counter/>

        <Users/>
    </div>
)

export default App
