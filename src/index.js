import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import {composeWithDevTools} from 'redux-devtools-extension';
import {applyMiddleware, createStore} from 'redux';
import {createEpicMiddleware} from "redux-observable";
import {ajax} from 'rxjs/ajax';
import 'bootstrap/dist/css/bootstrap.min.css';

import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import todoReducers from './reducers';
import todoEpics from './epics';

const epicMiddleware = createEpicMiddleware({
    dependencies: {getJSON: ajax.getJSON}
});
let store = createStore(todoReducers, composeWithDevTools(
    applyMiddleware(epicMiddleware)
));
epicMiddleware.run(todoEpics);

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>, document.getElementById('root')
);
registerServiceWorker();
