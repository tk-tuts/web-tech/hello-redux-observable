import {combineReducers} from 'redux';
import visibilityFilter from './visibility-filter';
import todos from './todos';
import counter from './counter';
import users from "./users";

const todoApp = combineReducers({
    visibilityFilter,
    todos,
    counter,
    users
});

export default todoApp;
