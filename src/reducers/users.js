import {FETCH_USER, FETCH_USER_FULFILLED} from "../constants/action-types";

const defaultState = {
    isLoading: false,
    user: null
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case FETCH_USER: {
            return {
                ...state,
                isLoading: true
            }
        }
        case FETCH_USER_FULFILLED: {
            return {
                isLoading: false,
                user: {
                    imageUrl: action.payload.avatar_url,
                    name: action.payload.name
                }
            };
        }
        default: {
            return state;
        }
    }
};
