import {INCREMENT_COUNTER} from "../constants/action-types";

export default (state = 0, action) => {
    switch (action.type) {
        case INCREMENT_COUNTER: {
            return ++state;
        }
        default: {
            return state;
        }
    }
};
